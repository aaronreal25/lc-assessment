# GVC Ladbrokes Coral Assesment Exam

## Live Demo

[CLICK HERE](https://aaronreal.gitlab.io/lc-assessment/)

## Tech
- Bootstrap
- FontAwesome
- Gulp
- jQuery

## Install npm and run gulp

```sh
npm install
gulp default
```

Check index file on `/public/index.html`