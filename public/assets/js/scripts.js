(function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({1:[function(require,module,exports){
if (navigator.userAgent.match(/IEMobile\/10\.0/)) {
  var msViewportStyle = document.createElement('style');
  msViewportStyle.appendChild(
    document.createTextNode(
      '@-ms-viewport{width:auto!important}'
    )
  );
  document.querySelector('head').appendChild(msViewportStyle);
}
//  * Prevents errors on console methods when no console present.
//  * Exposes a global 'log' function that preserves line numbering and formatting.
(function () {
  "use strict";
  var method,
    noop = function () { },
    methods = [
      'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
      'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
      'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
      'timeStamp', 'trace', 'warn'
    ],
    length = methods.length,
    console = (window.console = window.console || {});

  while (length--) {
    method = methods[length];

    // Only stub undefined methods.
    if (!console[method]) {
      console[method] = noop;
    }
  }

  if (Function.prototype.bind) {
    window.log = Function.prototype.bind.call(console.log, console);
  } else {
    window.log = function () {
      Function.prototype.apply.call(console.log, console, arguments);
    };
  }

  function creative() {
    setTimeout(console.log.bind(console, "%c© 2019 Creative Team - careers.ladbrokescoral.com", "font-size:10px;background: linear-gradient(to right, rgba(240,30,41,1) 0%,rgba(0,51,153,1) 100%);color:#fff;padding:4px;border-radius:4px;line-height: 1.1;", ""));
  }
  creative();

  $.fn.isInViewport = function() {
    var elementTop = $(this).offset().top;
    var elementBottom = elementTop + $(this).outerHeight();

    var viewportTop = $(window).scrollTop();
    var viewportBottom = viewportTop + $(window).height();

    return elementBottom > viewportTop && elementTop < viewportBottom;
  };

  $(window).on('resize scroll', function() {
    if ($('#hero--cta').isInViewport()) {
        $('#sticky--cta').removeClass('active');
    } else {
        $('#sticky--cta').addClass('active');
    }
  });

})();

},{}]},{},[1]);

//# sourceMappingURL=scripts.js.map
