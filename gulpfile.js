// Module
var path 		     = require('path');
var gulp 		     = require('gulp');
var gulpif           = require('gulp-if');
var gulputil         = require('gulp-util');
var gulpnotify       = require('gulp-notify');
var sass 		     = require('gulp-sass');
var nodeSassGlobbing = require('node-sass-globbing');
var minifyCss	     = require('gulp-minify-css');
var autoprefixer     = require('gulp-autoprefixer');
var sourcemaps	     = require('gulp-sourcemaps');
var concat           = require('gulp-concat');
var rename           = require('gulp-rename');
var vb               = require('vinyl-buffer');
var vss              = require('vinyl-source-stream');
var uglify           = require('gulp-uglify');
var browserify       = require('browserify');
var watchify         = require('watchify');
var watch            = require('gulp-watch');
var mergeStream      = require('merge-stream');
var _                = require('lodash');

// Enviroment
var PRODUCTION = gulputil.env.production === undefined ? false : gulputil.env.production;

// Notification

function NOTIFY (message) {
    this.title = 'Gulp';

    if(message)  return this.message(message);
}

NOTIFY.prototype.message = function (message) {

    return gulpnotify({
        title: this.title,
        message: message,            
        onLast: true
    });

};

NOTIFY.prototype.error = function (error, message) {

    gulpnotify.onError({
        title: this.title,
        message: message + ':' + error.message,
        onLast: true
    })(error);

    console.log(error);

};


// Tasks
var TASKS = {};

// Sass

TASKS.sass = function (src, dist) {

    var filename = path.basename(dist);

    return gulp.src(src)
        .pipe(gulpif(!PRODUCTION,sourcemaps.init()))
        .pipe(sass({importer: nodeSassGlobbing}))
        .on('error', function(error){

            new NOTIFY().error(error, 'SASS Compilation Failed');

            this.emit('end');

        })
        .pipe(gulpif(true,autoprefixer(["last 2 versions"])))
        .pipe(concat(filename))
        .pipe(gulpif(PRODUCTION,minifyCss()))
        .pipe(gulpif(PRODUCTION,rename({ suffix: '.min' })))
        .pipe(gulpif(!PRODUCTION,sourcemaps.write('./')))
        .pipe(gulp.dest(dist.replace(filename,'')))
        .pipe(new NOTIFY('SASS Compiled!'));

};

TASKS.javascript = function (src, dist) {

    var filename = path.basename(dist);

    return gulp.src(src)
        .pipe(gulpif(!PRODUCTION,sourcemaps.init()))
        .pipe(concat(filename))                
        .on('error', function(error){

            new NOTIFY().error(error, 'Javascript Compilation Failed');

            this.emit('end');

        })
        .pipe(gulpif(PRODUCTION,uglify()))
        .pipe(gulpif(PRODUCTION,rename({ suffix: '.min' })))
        .pipe(gulpif(!PRODUCTION,sourcemaps.write('./')))
        .pipe(gulp.dest(dist.replace(filename,'')))
        .pipe(new NOTIFY('Javascript Compiled!'));

};

// Browserify

TASKS.browserify = function (src, dist, watch) {

    watch = watch || false;

	var filename = path.basename(dist),

    watchify_args = _.assign(watchify.args, { debug: true }),

    bundler = watch ? watchify(browserify(src), watchify_args) : browserify(src),

    bundle = function () {
        return bundler
            .transform('require-globify')
            .transform('browserify-shim', {global: true})
            .bundle()
            .on('error', function (error) {

                new NOTIFY().error(error, 'Browserify Compilation Failed');

                this.emit('end');

            })
            .pipe(vss(filename))
            .pipe(vb())
            .pipe(gulpif(!PRODUCTION,sourcemaps.init({loadMaps: true}))) 
            .pipe(gulpif(PRODUCTION,uglify()))
            .pipe(gulpif(PRODUCTION,rename({ suffix: '.min' })))
            .pipe(gulpif(!PRODUCTION,sourcemaps.write('./')))
            .pipe(gulp.dest(dist.replace(filename,'')))
            .pipe(new NOTIFY('Browserify Compiled!'));
    }

    if ( !watch ) return bundle();

    bundle();
    bundler.on('update', bundle);

};

// Copy

TASKS.copy = function (src, dist) {
    return gulp.src(src)             
        .on('error', function (error) {

            new NOTIFY().error(error, 'Files Copying Failed');

            this.emit('end');

        })
        .pipe(gulp.dest(dist))
        .pipe(new NOTIFY('Files Copied!'));
};

// Task

gulp.task('copy',function () {
    var fonts = TASKS.copy('./src/fonts/**/*.{eot,svg,ttf,woff,woff2,otf}', './public/assets/fonts');
    var fontawesome = TASKS.copy('./node_modules/@fortawesome/fontawesome-free/webfonts/*.{eot,svg,ttf,woff,woff2,otf}', './public/assets/webfonts');
    var img = TASKS.copy('./src/img/**/*.{jpg,jpeg,gif,png,svg}', './public/assets/img');
    return mergeStream(fonts,fontawesome, img);
});

gulp.task('style',function () {
    return TASKS.sass('./src/scss/init.scss','./public/assets/css/styles.css');
});

gulp.task('script',function () {
    var vendor = TASKS.javascript([
        './node_modules/jquery/dist/jquery.js',
    ],'./public/assets/js/vendor.js');
	var app = TASKS.browserify('./src/js/scripts.js','./public/assets/js/scripts.js');
    return mergeStream(vendor,app);
});

gulp.task('default',['copy', 'style', 'script'], function () {
    watch('./src/scss/**/*.scss', function() {
        gulp.start('style');
    });
    TASKS.browserify('./src/js/scripts.js', './public/assets/js/scripts.js', !PRODUCTION);
});
